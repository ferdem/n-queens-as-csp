/**
 * A class that tests the performance of the solvers.
 * You can find the results in QuestionD.pdf.
 */
public class Driver {

	private final double timeLimit;

	/**
	 * Creates a new driver for limit for each iteration.
	 */
	public Driver(double timeLimit) {
		this.timeLimit = timeLimit;
	}
	
	/**
	 * Runs test for the Backtracker
	 */
	public void runTest() {
		Backtracker backtracker;
		for (int i = 4; ; i++) {
			NQueensPuzzle problem = new NQueensPuzzle(i);
			backtracker = new Backtracker(problem);
			
			double t0 = System.nanoTime();
			Assignment assg = backtracker.solve();
			double t1 = System.nanoTime() - t0;
			
			System.out.print("Size: " + i + ", ");
			System.out.println("Time : " + t1 / 1E9);
			//problem.print(assg);
			
			if (t1 > timeLimit) return;
		}
	}
	
	/**
	 * Runs test for the BacktrackerMRV
	 */
	public void runTestMRV() {
		Backtracker backtracker;
		for (int i = 4; ; i++) {
			NQueensPuzzle problem = new NQueensPuzzle(i);
			backtracker = new BacktrackerMRV(problem);
			
			double t0 = System.nanoTime();
			Assignment assg = backtracker.solve();
			double t1 = System.nanoTime() - t0;
			
			System.out.print("Size: " + i + ", ");
			System.out.println("Time : " + t1 / 1E9);
			//problem.print(assg);
			
			if (t1 > timeLimit) return;
		}
	}
	
	public static void main(String[] args) {
		Driver driver = new Driver(1E9); // 1 second limit
		System.out.println("Without any heuristic: ");
		driver.runTest();
		System.out.println("Using MRV heuristic: ");
		driver.runTestMRV();
	}
}
