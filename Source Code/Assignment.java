import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Encapsulates a partial assignment which assigns the queens into columns 
 * and keeps track of each variables domain.
 */
public class Assignment {

	private Map<NQueensPuzzle.Variable, Integer> assignments = null;
	private Map<NQueensPuzzle.Variable, Set<Integer>> domains = null;

	/**
	 * Creates an empty assignment.
	 */
	public Assignment() {
		assignments = new HashMap<>();
		domains = new HashMap<>();
	}

	/**
	 * Creates a new assignment from this one by assigning variable v
	 * into the i'th column.
	 */
	public Assignment assign(NQueensPuzzle.Variable v, Integer i) {
		// copy this assignment
		Assignment newAssg = new Assignment();
		newAssg.assignments.putAll(assignments);
		newAssg.domains.putAll(domains);
		// put new
		newAssg.assignments.put(v, i);
		// restrict the domain
		Set<Integer> domain = new HashSet<>(Arrays.asList(i));
		newAssg.domains.put(v, domain); 
		return newAssg;
	}

	/**
	 * Returns the place where queen v is assigned to.
	 */
	public Integer getValue(NQueensPuzzle.Variable v) {
		return assignments.get(v);
	}

	/**
	 * Returns the domain of the variable. Notice it returns null if
	 * it is restricted.
	 */
	public Set<Integer> getDomain(NQueensPuzzle.Variable v) {
		return domains.get(v);
	}

	/**
	 * Sets the domain of the variable v to the domain d. 	 
	 */
	public void restrictDomain(NQueensPuzzle.Variable v, Set<Integer> d) {
		domains.put(v, d);
	}

}
