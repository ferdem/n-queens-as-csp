import java.util.ArrayList;
import java.util.List;

/**
 * Improves the regular backtracker with forward chaining using MRV heuristic.
 * Performance of this heuristic is argued at QuestionD.pdf
 */
public class BacktrackerMRV extends Backtracker {

	private NQueensPuzzle.Variable min;
	
	public BacktrackerMRV(NQueensPuzzle problem) {
		super(problem);
	}
	
	/**
	 * Returns the unassigned variable in the partial assignment 'assg'
	 * with minimum remaining values.
	 */
	protected NQueensPuzzle.Variable getUnassignedVariable(Assignment assg) {
		List<NQueensPuzzle.Variable> unassigned = new ArrayList<>();
		for (NQueensPuzzle.Variable v : problem.variables)
			if (assg.getValue(v) == null)
				unassigned.add(v);
		if (unassigned.isEmpty())
			return null;
		min = unassigned.get(0);
		for (int i = 1; i < unassigned.size(); i++) 
			if (compare(assg, unassigned.get(i)))
				min = unassigned.get(i);
		return min;
	}

	/**
	 * Used to compare the variables remaining values.
	 */
	private boolean compare(Assignment assg, NQueensPuzzle.Variable var) {
		return problem.getDomain(assg, var).size() < 
				problem.getDomain(assg, min).size();
	}

	/**
	 * Solutions for n = 4, 5, 6, 7, 8
	 */
	public static void main(String[] args) {
		for (int n = 4; n <= 8; n++) {
			NQueensPuzzle puzzle = new NQueensPuzzle(n);
			Backtracker backtracker = new Backtracker(puzzle);
			Assignment assg = backtracker.solve();
			System.out.println("N = " + n);
			puzzle.print(assg);
			System.out.println();
		}
	}
}
