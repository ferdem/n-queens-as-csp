import java.util.ArrayList;
import java.util.List;

/**
 * Solves a given N-Queens Puzzle using backtracking with forward checking. 
 */
public class Backtracker {

	protected NQueensPuzzle problem;

	public Backtracker(NQueensPuzzle problem) {
		this.problem = problem;
	}

	/**
	 * Solves and returns the solution.
	 */
	public Assignment solve() {
		return recursiveSolve(new Assignment()); // start with empty
	}
	
	/**
	 * Recursively calls this function until the constraint is satisfied by the
	 * given assignment.	 
	 */
	private Assignment recursiveSolve(Assignment assg) {
		if (problem.constraint.isSatisfied(assg)) 
			return assg; // success
		
		NQueensPuzzle.Variable v = getUnassignedVariable(assg);
		if (v == null) return null;
		
		List<Integer> values = new ArrayList<>(problem.getDomain(assg, v));
		
		for (Integer value: values) {
			Assignment newAssg = assg.assign(v, value);
			problem.forwardCheck(newAssg, v); // forward checking 
			
			if (!problem.constraint.isConsistent(newAssg)) 
				continue; // give up on path 
			
			newAssg = recursiveSolve(newAssg);
			if (newAssg != null) return newAssg;
		}
		return null; // fails
	}

	/**
	 * Returns an unassigned variable in the partial assignment 'assg'.
	 */
	protected NQueensPuzzle.Variable getUnassignedVariable(Assignment assg) {
		for (NQueensPuzzle.Variable v : problem.variables)
		      if (assg.getValue(v) == null) return v;
		    return null;
	}

}
