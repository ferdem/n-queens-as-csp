import static java.lang.Math.abs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Bonus Question:
 * Solves the NQueesPuzzle using Hill Climbing.
 */
public class HillClimber {

	protected NQueensPuzzle problem;

	public HillClimber(NQueensPuzzle problem) {
		this.problem = problem;
	}
	
	/**
	 * Solves and returns the solution.
	 */
	public Assignment solve() {
		Assignment assg = new Assignment();
		int n = problem.variables.size();

		for (int i = 0; i < n; i++) {
			NQueensPuzzle.Variable var = problem.variables.get(i);
			assg = assg.assign(var, 0);
		}

		return recursiveSolve(assg);
	}

	private static Assignment oldAssg = null;

	/**
	 * Recursively calls this function until the constraint is satisfied by the
	 * given assignment.	 
	 */
	private Assignment recursiveSolve(Assignment assg) {
		if (problem.constraint.isSatisfied(assg))
			return assg; // success

		// adds possible moves to a list
		List<Assignment> neighbors = new ArrayList<>();
		for (int i = 0; i < problem.variables.size(); i++) {
			NQueensPuzzle.Variable var = problem.variables.get(i);
			int row = assg.getValue(var);
			if (row < problem.variables.size() - 1)
				neighbors.add(assg.assign(var, row + 1));
			if (row > 0)
				neighbors.add(assg.assign(var, row - 1));
		}

		// not to stuck on a shoulder.
		Assignment newAssg = null;
		if (oldAssg == null) {
			newAssg = Collections.min(neighbors, new MinConflict());
		} else {
			while (!neighbors.isEmpty()) {
				newAssg = Collections.min(neighbors, new MinConflict());
				if (isSame(oldAssg, newAssg))
					neighbors.remove(newAssg);
				else
					break;
			}
		}

		oldAssg = assg;
		return recursiveSolve(newAssg);
	}

	/**
	 * Checks if the two given assignments are same.
	 */
	private boolean isSame(Assignment assg1, Assignment assg2) {
		for (NQueensPuzzle.Variable var : problem.variables) {
			int i = assg1.getValue(var);
			int j = assg2.getValue(var);
			if (i != j)
				return false;
		}
		return true;
	}

	/**
	 * Implements Minimum Conflict Heuristic.
	 */
	private class MinConflict implements Comparator<Assignment> {
		@Override
		public int compare(Assignment o1, Assignment o2) {
			return Integer.compare(minconflict(o1), minconflict(o2));
		}
		
		private int minconflict(Assignment assg) {
			int counter = 0;
			for (int i = 0; i < problem.variables.size(); i++) {
				NQueensPuzzle.Variable outer = problem.variables.get(i);
				int outerRow = assg.getValue(outer);
				int outerColumn = outer.column;

				for (int j = i + 1; j < problem.variables.size(); j++) {
					NQueensPuzzle.Variable inner = problem.variables.get(j);
					int innerRow = assg.getValue(inner);
					int innerColumn = inner.column;
					if (innerRow == outerRow)
						counter++;
					if (abs(outerRow - innerRow) == abs(outerColumn - innerColumn))
						counter++;
				}
			}
			return counter;
		}
	}

	/**
	 * Solves and print 4-Queens puzzle.
	 */
	public static void main(String[] args) {
		NQueensPuzzle n = new NQueensPuzzle(4);
		HillClimber h = new HillClimber(n);
		Assignment a = h.solve();
		n.print(a);
	}
}
