import static java.lang.Math.abs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Encapsulates an N-Queen Puzzle as a Constraint Satisfaction Problem.
 */
public class NQueensPuzzle {
	public Constraint constraint;
	public List<Variable> variables; 
	private Set<Integer> initialDomain;
	/**
	 * Encapsulates a variable of the CSP, which is a queen.
	 */
	public class Variable {
		public int column;
		
		public Variable(int column) {
			this.column = column;
		}
	}
	/**
	 * Encapsulates a constraint in the CSP.	 
	 */
	public class Constraint {
		/**
		 * Checks if the assignment satisfies the constraint in this CSP.
		 */
		public boolean isSatisfied(Assignment assg) {
			for (int i = 0; i < variables.size(); i++) {
				Variable outer = variables.get(i);
				Integer outerRow = assg.getValue(outer);
				Integer outerColumn = ((Variable) outer).column;
				if (outerRow == null)
					return false;

				for (int j = i + 1; j < variables.size(); j++) {
					Variable inner = variables.get(j);
					Integer innerRow = assg.getValue(inner);
					Integer innerColumn = inner.column;
					if (innerRow == null)
						return false;

					if (innerRow == outerRow)
						return false;
					if (abs(outerRow - innerRow) == abs(outerColumn - innerColumn))
						return false;
				}
			}
			return true;
		}
		/**
		 * Checks if the given assignment is consistent with the constraint 
		 * in this CSP. Notice it does not necessarily satisfies the constraint.
		 */
		public boolean isConsistent(Assignment assg) {
			for (int i = 0; i < variables.size(); i++)
				if (getDomain(assg, variables.get(i)).isEmpty())
					return false;
			return true;
		}
	}
	/**
	 * Creates a new N-Queen Puzzle of size n.
	 */
	public NQueensPuzzle(int n) {
		constraint = new Constraint();
		initialDomain = new HashSet<>(n);
		for (int i = 0; i < n; i++)
			initialDomain.add(i);

		variables = new ArrayList<>(n);
		for (int i = 0; i < n; i++)
			variables.add(new Variable(i));
	}
	/**
	 * Returns the domain of the variable 'var' when 'assg' is given as
	 * an assignment to this CSP.
	 */
	public Set<Integer> getDomain(Assignment assg, Variable var) {
		Set<Integer> domain = assg.getDomain(var);
		if (domain != null)
			return domain;
		return initialDomain;
	}
	/**
	 * Implements a forward checking method. It restricts the domains of the
	 * variables in the assignment 'assg' using where the variable 'var' is 
	 * assigned to in this CSP.
	 */
	public void forwardCheck(Assignment assg, Variable var) {
		Integer i = ((Variable) var).column;
		Integer r = assg.getValue(var); // row assigned

		for (int j = 0; j < variables.size(); j++) {
			if (i == j)
				continue;
			Variable v = variables.get(j);
			Set<Integer> dom = new HashSet<>(getDomain(assg, v));

			dom.remove(r);
			dom.remove(r - (j - i));
			dom.remove(r + (j - i));

			assg.restrictDomain(v, dom);
		}
	}
	/**
	 * Prints the assignment 'assg' on this CSP. 
	 * @throws NullPointerException when 'assg' is a partial assignment.
	 */
	public void print(Assignment assg) {
		for (int i = 0; i < variables.size(); i++) {
			for (int j = 0; j < variables.size(); j++) {
				Integer k = assg.getValue(variables.get(i));
				if (k == j)
					System.out.print("Q");
				else
					System.out.print("*");
			}
			System.out.println();
		}
	}
}
